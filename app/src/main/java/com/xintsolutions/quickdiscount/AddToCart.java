package com.xintsolutions.quickdiscount;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;
import com.xintsolutions.quickdiscount.utils.Utils2;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Asad Waheed on 10/9/2016.
 */

public class AddToCart extends AppCompatActivity {

    Button cut_button_quantity, add_button_quantity;
    TextView tvWeight;
    AppCompatActivity mActivity;
    Product product;
    int quantity = 1;
    TextView tvTotalAmount;
    int price;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addtocart);
        mActivity = this;
        cut_button_quantity = (Button) findViewById(R.id.cut_quantity);
        add_button_quantity = (Button) findViewById(R.id.add_quantity);
        TextView tvProdName = (TextView) findViewById(R.id.tvProdName);
        tvWeight = (TextView) findViewById(R.id.weight);
        TextView tvUnitPrice = (TextView) findViewById(R.id.tvUnitPrice);
        tvTotalAmount = (TextView) findViewById(R.id.tvAmount);
        ImageView ivProdImg = (ImageView) findViewById(R.id.ivProdImg);
        Button btnAddToCart = (Button) findViewById(R.id.btnAddToCart);
        product = (Product) getIntent().getExtras().getSerializable("product");
        Glide.with(mActivity).load(product.image).placeholder(new ColorDrawable(0xEEEEEE))
                .into(ivProdImg);
        tvProdName.setText(product.title);
        price = Math.round(Float.parseFloat(product.price));
        tvUnitPrice.setText(product.currency + ". " + product.price);
        tvTotalAmount.setText(product.currency + ". " + product.price);
        cut_button_quantity.setOnClickListener(clickListener);
        add_button_quantity.setOnClickListener(clickListener);
        btnAddToCart.setOnClickListener(clickListener);
    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return false;
    }*/
    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cut_quantity:
                    if (quantity > 1) {
                        quantity--;
                        tvTotalAmount.setText(product.currency + ". " + (price * quantity) + "");
                        tvWeight.setText(quantity + "");
                    }
                    break;
                case R.id.add_quantity:
                    quantity++;
                    tvWeight.setText(quantity + "");
                    tvTotalAmount.setText(product.currency + ". " + (price * quantity) + "");
                    break;
                case R.id.btnAddToCart:
                    List<Product> listProducts = Settings.getCartList(mActivity);
                    if (listProducts == null)
                        listProducts = new ArrayList<>();
                    product.quantity = quantity;
                    product.priceOfOrder = (price * quantity);
                    listProducts.add(product);
                    Settings.setCartList(mActivity, listProducts);
                    startActivity(new Intent(mActivity, CheckoutAct.class));
                    break;
            }
        }
    };

}
