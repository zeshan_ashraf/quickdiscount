package com.xintsolutions.quickdiscount.model;

import java.io.Serializable;

/**
 * Created by Asad Waheed on 10-Nov-16.
 */

public class LineItems implements Serializable{
    public String name ;
    public String product_img ;
    public String quantity ;
    public String price ;
    public String currency ;
    public String total ;
    public String id;
}
