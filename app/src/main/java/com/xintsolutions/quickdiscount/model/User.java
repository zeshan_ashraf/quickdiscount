package com.xintsolutions.quickdiscount.model;

/**
 * Created by Asad Waheed on 27-Oct-16.
 */

public class User {
    public String user_id ;
    public String email_address ;
    public String user_name ;
    public String address ;
    public String phone_number ;
    public String first_name ;
    public String last_name ;
    public String image;
    public String code;
}
