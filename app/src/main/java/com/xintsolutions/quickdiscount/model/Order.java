package com.xintsolutions.quickdiscount.model;

import java.util.ArrayList;

/**
 * Created by Asad Waheed on 10-Nov-16.
 */

public class Order {
    public int id;
    public String date;
    public String status;
    public String currency;
    public String total;
    public ArrayList<LineItems> line_items;
}
