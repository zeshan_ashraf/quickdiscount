package com.xintsolutions.quickdiscount.model;

import java.io.Serializable;

/**
 * Created by Asad Waheed on 14-Nov-16.
 */

public class UserInfo implements Serializable{
    public String email_address ;
    public String user_name ;
    public String address ;
    public String phone_number ;
    public String fname ;
    public String lname ;
    public String postal_code ;
    public String address2 ;
    public String city;
    public String state;
    public String country;
}
