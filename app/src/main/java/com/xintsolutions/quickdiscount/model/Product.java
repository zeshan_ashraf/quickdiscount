package com.xintsolutions.quickdiscount.model;

import java.io.Serializable;

/**
 * Created by Asad Waheed on 21-Sep-16.
 */

public class Product implements Serializable {


    public String title;
    public String image;
    public String price;
    public String id;
    public String type;
    public String currency;

    //app side fields
    public int quantity;
    public int priceOfOrder;
}
