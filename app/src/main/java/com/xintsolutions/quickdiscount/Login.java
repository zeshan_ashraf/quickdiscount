package com.xintsolutions.quickdiscount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;
import com.xintsolutions.quickdiscount.model.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hp on 10/3/2016.
 */

public class Login extends AppCompatActivity {

    Button login;
    AppCompatActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        mActivity = this;
        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Add your code in here!
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    Toast.makeText(mActivity, "A required field is missing.", Toast.LENGTH_SHORT).show();
                }else{
                    new AsyncLogin().execute(username,password);
                }
                /*Intent intent = new Intent(Login.this, MainScreen.class);
                startActivity(intent);*/
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
    }

    ProgressDialog mProgressDialog;

    private class AsyncLogin extends AsyncTask<String, Long, String> {
        public String phone;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                phone= urls[0];
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "login", "username", urls[0],"password",urls[1]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        Settings.setIsUserLoggedIn(true,mActivity);
                        JSONObject msgObj = jsonObject.getJSONObject("user");
                        User user = new User();
                        user.user_id=msgObj.getString("user_id");
                        user.email_address=msgObj.getString("email_address");
                        user.user_name=msgObj.getString("user_name");
                        user.address=msgObj.getString("address");
                        user.phone_number=msgObj.getString("phone_number");
                        user.image=msgObj.getString("user_image");
                        Settings.setUserId(user.user_id,mActivity);
                        Settings.setUser(mActivity,user);
                        startActivity(new Intent(mActivity, DrawerActivity.class));
                        finish();
                    }else{
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
