package com.xintsolutions.quickdiscount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.adapter.ListAdapterProducts;
import com.xintsolutions.quickdiscount.adapter.MyItemRecyclerViewAdapterThirdTab;
import com.xintsolutions.quickdiscount.model.Category;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hp on 10/26/2016.
 */

public class Products extends AppCompatActivity {

    Products mActivity;
    ArrayList<Product> listTips;
    ArrayList listFiltered;
    public static int REP_DELAY = 50;
    private Handler repeatUpdateHandler = new Handler();
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;
    private boolean mAutoMaxIncrement = false;
    private boolean mAutoMaxDecrement = false;
    ListView lvProduct;
    TextView minprice, maxPrice;
    int price = 0;
    int priceMax = 0;
    public static String id;
    public static String title;
    ArrayList<Product> listTipsFiltered;
    public SearchView etURL;
    int textlength;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product);
        mActivity = this;
        lvProduct = (ListView) findViewById(R.id.lvProduct);
        findViewById(R.id.min_dec_price).setOnClickListener(clickListener);
        findViewById(R.id.min_inc_price).setOnClickListener(clickListener);
        findViewById(R.id.max_dec_price).setOnClickListener(clickListener);
        findViewById(R.id.max_inc_price).setOnClickListener(clickListener);
        findViewById(R.id.max_inc_price).setOnLongClickListener(onLongClickListener);
        findViewById(R.id.max_dec_price).setOnLongClickListener(onLongClickListener);
        findViewById(R.id.min_inc_price).setOnLongClickListener(onLongClickListener);
        findViewById(R.id.min_dec_price).setOnLongClickListener(onLongClickListener);
        findViewById(R.id.max_inc_price).setOnTouchListener(onTouchListener);
        findViewById(R.id.max_dec_price).setOnTouchListener(onTouchListener);
        findViewById(R.id.min_inc_price).setOnTouchListener(onTouchListener);
        findViewById(R.id.min_dec_price).setOnTouchListener(onTouchListener);
        etURL = (SearchView) findViewById(R.id.etURL);
        findViewById(R.id.btnApply).setOnClickListener(clickListener);
        minprice = (TextView) findViewById(R.id.minprice);
        maxPrice = (TextView) findViewById(R.id.maxPrice);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            id = b.getString("id");
            title = b.getString("title");

        }
        setTitle(title);
        listTipsFiltered = new ArrayList<>();
        if (listTips == null) {
            new AsyncGetProducts().execute(id);
        } else {
            lvProduct.setAdapter(new ListAdapterProducts(
                    mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
        }
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product product = (Product) lvProduct.getAdapter().getItem(i);
                Intent intent = new Intent(mActivity, AddToCart.class);
                intent.putExtra("product", product);
                startActivity(intent);
            }
        });
        etURL.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                textlength = etURL.getQuery().length();
                String searchInput = etURL.getQuery().toString();
                if (textlength > 0) {
                    if (!listTips.isEmpty()) {
                        listTipsFiltered.clear();
                        for (int i = 0; i < listTips.size(); i++) {
                            if (listTips.get(i).title.toLowerCase().contains(
                                    searchInput.toLowerCase())) {
                                listTipsFiltered.add(listTips.get(i));
                            }
                        }
                    }
                    lvProduct.setAdapter(new ListAdapterProducts(
                            mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTipsFiltered));
                    return true;
                } else {
                    lvProduct.setAdapter(new ListAdapterProducts(
                            mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
                    return true;
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putString("id", id);
        outState.putString("title", title);
        super.onSaveInstanceState(outState, outPersistentState);
    }
  /*  protected void onSaveInstanceState(Bundle outState){
        outState.putString("id", id);
        outState.putString("title", title);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            id=savedInstanceState.getString("id");
            title=savedInstanceState.getString("title");
        }
    }*/

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.min_dec_price:
                    if (price >= 1) {
                        price--;
                        minprice.setText(price + "");
                    }
                    break;
                case R.id.min_inc_price:
                    price++;
                    if (price > priceMax)
                        priceMax = price;
                    minprice.setText(price + "");
                    break;
                case R.id.max_dec_price:
                    if (priceMax >= 1) {
                        priceMax--;
                        if (priceMax < price)
                            price = priceMax;
                        maxPrice.setText(priceMax + "");
                    }
                    break;
                case R.id.max_inc_price:
                    priceMax++;
                    maxPrice.setText(priceMax + "");
                    break;
            }
            if (price == 0 && priceMax == 0) {
                lvProduct.setAdapter(new ListAdapterProducts(
                        mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
            } else
                updateValues();
        }
    };

    private void updateValues() {
        listFiltered = new ArrayList();
        int loopPrice;
        if (listTips != null) {
            for (int i = 0; i < listTips.size(); i++) {
                loopPrice = Math.round(Float.parseFloat(listTips.get(i).price));
                if (loopPrice >= price && loopPrice <= priceMax) {
                    listFiltered.add(listTips.get(i));
                }
            }
            if (listFiltered.size() > 0) {
                lvProduct.setAdapter(new ListAdapterProducts(
                        mActivity, R.layout.row_list_frnds, R.id.tvProdName, listFiltered));
            } else {
                /*Toast.makeText(mActivity, "Your selected filters returned no matches."
                        , Toast.LENGTH_SHORT).show();*/
                lvProduct.setAdapter(null);
            }
        }
    }

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            switch (view.getId()) {
                case R.id.min_dec_price:
                    mAutoDecrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.min_inc_price:
                    mAutoIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.max_dec_price:
                    mAutoMaxDecrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.max_inc_price:
                    mAutoMaxIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
            }
            return false;
        }
    };
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                //&& mAutoIncrement
                    ) {
                mAutoIncrement = false;
                mAutoMaxDecrement = false;
                mAutoMaxIncrement = false;
                mAutoDecrement = false;
                if (price == 0 && priceMax == 0) {
                    lvProduct.setAdapter(new ListAdapterProducts(
                            mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
                } else
                    updateValues();
            }
            return false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return false;
    }

    ProgressDialog mProgressDialog;

    private class AsyncGetProducts extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "category_products", "cat_name", urls[0]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        JSONArray msgObj = jsonObject.getJSONArray("category_products");
                        int size = msgObj.length();
                        listTips = new ArrayList<>();
                        Product cat;
                        JSONObject jObj;
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                cat = new Product();
                                jObj = msgObj.getJSONObject(i);
                                cat.id = jObj.getString("id");
                                cat.title = jObj.getString("title");
                                cat.image = jObj.getString("image");
                                cat.type = jObj.getString("type");
                                cat.price = jObj.getString("price");
                                cat.currency = jObj.getString("currency");
                                listTips.add(cat);
                            }
                        }
                        lvProduct.setAdapter(new ListAdapterProducts(
                                mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class RptUpdater implements Runnable {
        public void run() {
            if (mAutoIncrement) {
                incrementMin();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoDecrement) {
                decrementMin();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoMaxIncrement) {
                incrementMax();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoMaxDecrement) {
                decrementMax();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            }
        }
    }

    public void decrementMin() {
        if (price >= 1) {
            price--;
            minprice.setText(price + "");
        }
    }

    public void incrementMin() {
        price++;
        if (price > priceMax)
            priceMax = price;
        minprice.setText(price + "");
    }

    public void decrementMax() {
        if (priceMax >= 1) {
            priceMax--;
            if (priceMax < price)
                price = priceMax;
            maxPrice.setText(priceMax + "");
        }
    }

    public void incrementMax() {
        priceMax++;
        maxPrice.setText(priceMax + "");
    }
}
