package com.xintsolutions.quickdiscount;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.utils.Settings;

public class Splash extends AppCompatActivity {
    AppCompatActivity mContext;
    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        mContext = this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
             ////   if (Settings.getIsUserLoggedIn(mContext)){
                    startActivity(new Intent(mContext, DrawerActivity.class));
          //      }else{
           //         startActivity(new Intent(mContext, MainActivity.class));
          //      }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
