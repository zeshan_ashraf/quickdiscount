package com.xintsolutions.quickdiscount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.activity.LoginActivity;
import com.xintsolutions.quickdiscount.activity.PaymentMethod;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.model.UserInfo;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hp on 10/3/2016.
 */

public class UserInfoForm extends AppCompatActivity {
    AppCompatActivity mActivity;
    TextView etAddressTwo;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info);
        final EditText etFName = (EditText) findViewById(R.id.etFName);
        final EditText etLName = (EditText) findViewById(R.id.etLName);
        final EditText etEmail = (EditText) findViewById(R.id.etEmail);
        final EditText etPhone = (EditText) findViewById(R.id.etPhone);
        final EditText etPostalCode = (EditText) findViewById(R.id.etPostalCode);
        final EditText etAddressOne = (EditText) findViewById(R.id.etAddressOne);
        etAddressTwo = (TextView) findViewById(R.id.etAddressTwo);
        final EditText etCity = (EditText) findViewById(R.id.etCity);
        final EditText etState = (EditText) findViewById(R.id.etState);
        final EditText etCountry = (EditText) findViewById(R.id.etCountry);
        Button btnSignUp = (Button) findViewById(R.id.etSignUp);
        mActivity = this;
        User user = Settings.getUser(mActivity);
        if(user!=null) {
            etFName.setText(user.first_name);
            etLName.setText(user.last_name);
            etEmail.setText(user.email_address);
            etPhone.setText(user.phone_number);
            etAddressOne.setText(user.address);
            etAddressTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent =
                                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                        .build(mActivity);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                        // TODO: Handle the error.
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        // TODO: Handle the error.
                    }
                }
            });
            btnSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String fname = etFName.getText().toString();
                    String lastName = etLName.getText().toString();
                    String phone = etPhone.getText().toString();
                    String city = etCity.getText().toString();
                    String state = etState.getText().toString();
                    String postalCode = etPostalCode.getText().toString();
                    String email = etEmail.getText().toString();
                    String addressOne = etAddressOne.getText().toString();
                    String addressTwo = etAddressTwo.getText().toString();
                    String country = etCountry.getText().toString();
                    boolean cancel = false;
                    if (TextUtils.isEmpty(fname) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email) ||
                            TextUtils.isEmpty(phone) || TextUtils.isEmpty(city) ||
                            TextUtils.isEmpty(state) || TextUtils.isEmpty(postalCode) || TextUtils.isEmpty(addressOne)
                            || TextUtils.isEmpty(country) || TextUtils.isEmpty(addressTwo)) {
                        Toast.makeText(mActivity, "A required field is missing.", Toast.LENGTH_SHORT)
                                .show();
                        cancel = true;
                    }
                    if (!cancel) {
                        UserInfo info = new UserInfo();
                        info.fname = fname;
                        info.lname = lastName;
                        info.phone_number = phone;
                        info.city = city;
                        info.state = state;
                        info.postal_code = postalCode;
                        info.email_address = email;
                        info.address = addressOne;
                        info.address2 = addressTwo;
                        info.country = country;
                        Intent intent = new Intent(mActivity, PaymentMethod.class);
                        intent.putExtra("data", info);
                        startActivity(intent);
                  /*  new AsyncSignUp().execute(fname, lastName, phone, city, state, postalCode, email, addressOne,
                            addressTwo, country);*/
                    }
                }
            });
        }else{
            Toast.makeText(mActivity,"Please login to use checkout.",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(mActivity, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                etAddressTwo.setText(place.getName());
                Log.i("User address", "Place: " + place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("User address", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(UserInfoForm.this, MainActivity.class);
        startActivity(intent);*/
    }


}
