package com.xintsolutions.quickdiscount;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.xintsolutions.quickdiscount.adapter.ListAdapterOrderItems;
import com.xintsolutions.quickdiscount.model.LineItems;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asad Waheed on 10/26/2016.
 */

public class OrderItemsAct extends AppCompatActivity {

    AppCompatActivity mActivity;
    ListView lvProduct;
    ArrayList<LineItems> listLineItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_items);
        mActivity = this;
        setTitle("Items in this Order");
        lvProduct = (ListView) findViewById(R.id.lvOrder);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            listLineItems = (ArrayList<LineItems>) b.getSerializable("order");
            lvProduct.setAdapter(new ListAdapterOrderItems(mActivity, R.layout.row_list_items_in_order,
                    R.id.tvItemName, listLineItems));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_items, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuReOrder) {
            int size = listLineItems.size();
            if (size > 0) {
                List<Product> listProducts = Settings.getCartList(mActivity);
                if (listProducts == null)
                    listProducts = new ArrayList<>();
                Product product;
                LineItems items;
                for (int i = 0; i < size; i++) {
                    product = new Product();
                    items = listLineItems.get(i);
                    product.quantity = Integer.parseInt(items.quantity);
                    product.id = items.currency;
                    product.currency = items.currency;
                    product.title = items.name;
                    product.price = items.price;
                    product.image = items.product_img;
                    product.priceOfOrder = (int)Double.parseDouble(items.total);
                    listProducts.add(product);
                }

                Settings.setCartList(mActivity, listProducts);
                startActivity(new Intent(mActivity, CheckoutAct.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }
    //ProgressDialog mProgressDialog;

   /* private class AsyncGetProducts extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "category_products", "cat_name", urls[0]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        JSONArray msgObj = jsonObject.getJSONArray("category_products");
                        int size = msgObj.length();
                        listTips = new ArrayList<>();
                        Product cat;
                        JSONObject jObj;
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                cat = new Product();
                                jObj = msgObj.getJSONObject(i);
                                cat.id = jObj.getString("id");
                                cat.title = jObj.getString("title");
                                cat.image = jObj.getString("image");
                                cat.type = jObj.getString("type");
                                cat.price = jObj.getString("price");
                                cat.currency = jObj.getString("currency");
                                listTips.add(cat);
                            }
                        }
                        lvProduct.setAdapter(new ListAdapterProducts(
                                mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }*/

}
