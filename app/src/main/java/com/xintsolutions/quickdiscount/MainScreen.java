package com.xintsolutions.quickdiscount;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.xintsolutions.quickdiscount.adapter.TabsNavAdapter;
import com.xintsolutions.quickdiscount.fragment.MainFragment;

/**
 * Created by hp on 10/4/2016.
 */

public class MainScreen extends Fragment {
    private TabsNavAdapter adapter;
    private SmartTabLayout tabs;
    public ViewPager pager;
    AppCompatActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.main_screen,container,false);
        initUI(v);
        return v;
    }

    //tabs implementation
    private void initUI(View v) {
        tabs = (SmartTabLayout) v.findViewById(R.id.tabs);
        pager = (ViewPager) v.findViewById(R.id.pager);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                View tabView = LayoutInflater.from(mActivity).inflate(R.layout.layout, container, false);;
                ImageView ivTabImage = (ImageView) tabView.findViewById(R.id.ivTab);
                TextView tvTab = (TextView) tabView.findViewById(R.id.tvTab);
               /* if(tabView.gets)){
                    tvTab.setTextColor(ContextCompat.getColor(mActivity,R.color.tabs_text_color));
                }*/
                tvTab.setText(adapter.getPageTitle(position));
                switch (position) {
                    case 0:
                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.home));
                        break;
                    case 1:
                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.shop));
                        break;
                    case 2:
                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.order));
                        break;
                    case 3:
                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.azprod));
                        break;

                    default:
                        throw new IllegalStateException("Invalid position: " + position);
                }
                return tabView;
            }
        });
        adapter = new TabsNavAdapter(mActivity,getChildFragmentManager());
        pager.setAdapter(adapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        tabs.setViewPager(pager);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mActivity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }
    //above tabs implementation


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {

//        super.onCreateOptionsMenu(menu);
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_screen_menu, menu);
//        MenuItem searchItem = menu.findItem(R.id.menu_item_search_1);
//        SearchView searchView = (SearchView) searchItem.getActionView();


//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        if(null!=searchManager ) {
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        }
//        searchView.setIconifiedByDefault(false);
//
//        return true;
//    }
}
