package com.xintsolutions.quickdiscount.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class Settings {
    private static Gson gson;

    /*public static String getRegId(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(Constants.PROPERTY_REG_ID,"");
    }
    public static void setRegId(String regId,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.PROPERTY_REG_ID,regId);
        editor.commit();
    }*/
    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static User getUser(Context context) {
        if (gson == null) {
            gson = new Gson();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        String json = preferences.getString("newUserKey", "NA");
        if (json.equals("NA"))
            return null;
        Log.e("User", "User data from prefs: " + json);
        return gson.fromJson(json, User.class);
    }

    public static void setUser(Context context, User userData) {
        if (gson == null) {
            gson = new Gson();
        }
        String Usrdata;
        if (userData != null)
            Usrdata = gson.toJson(userData);
        else
            Usrdata = "NA";
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        preferences.edit().putString("newUserKey", Usrdata).apply();
    }

    public static List<Product> getCartList(Context context) {
        if (gson == null) {
            gson = new Gson();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        String json = preferences.getString("cartList", "NA");
        if (json.equals("NA"))
            return null;
        try {
            JSONArray array = new JSONArray(json);
            if (array != null) {
                Type listType = new TypeToken<List<Product>>() {
                }.getType();
                return gson.fromJson(array.toString(), listType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
        //return gson.fromJson(json, User.class);
    }

    public static void setCartList(Context context, List<Product> userData) {
        if (gson == null) {
            gson = new Gson();
        }
        String Usrdata = gson.toJson(userData, new TypeToken<ArrayList<Product>>() {
        }.getType());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        preferences.edit().putString("cartList", Usrdata).apply();
    }

    public static List<Product> getFavortList(Context context) {
        if (gson == null) {
            gson = new Gson();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        String json = preferences.getString("fvrtlist", "NA");
        if (json.equals("NA"))
            return null;
        try {
            JSONArray array = new JSONArray(json);
            if (array != null) {
                Type listType = new TypeToken<List<Product>>() {
                }.getType();
                return gson.fromJson(array.toString(), listType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
        //return gson.fromJson(json, User.class);
    }

    public static void setFavortList(Context context, List<Product> userData) {
        if (gson == null) {
            gson = new Gson();
        }
        String Usrdata = gson.toJson(userData, new TypeToken<ArrayList<Product>>() {
        }.getType());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        preferences.edit().putString("fvrtlist", Usrdata).apply();
    }

    public static String getUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("userId", "");
    }

    public static void setUserId(String regId, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("userId", regId);
        editor.commit();
    }

    public static String getUserType(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("userType", "");
    }

    public static void setUserType(String type, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("userType", type);
        editor.commit();
    }
    /*public static int getAppVersion(Context context){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getInt(Constants.PROPERTY_APP_VERSION,Integer.MIN_VALUE);
	}
	public static void setAppVersion(int appVersion,Context context){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(Constants.PROPERTY_APP_VERSION,appVersion);
		editor.commit();
	}*/

    /*public static void setUserProfile(FbProfile info,Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(info);
        editor.putString("profile", json);
        editor.commit();
    }
    public static BasicFields getPropertyFields(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString("basicFields", "");
        return gson.fromJson(json, BasicFields.class);
    }*/
    public static String getUDID(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    public static boolean getIsUserLoggedIn(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean("loggedIn", false);
    }

    public static void setIsUserLoggedIn(boolean isUserLoggedIn, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("loggedIn", isUserLoggedIn);
        editor.commit();
    }

    public static String getCountry(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("country", "");
    }

    public static void setCountry(String isUserLoggedIn, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("country", isUserLoggedIn);
        editor.commit();
    }

    public static String getSesionId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("countryID", "");
    }

    public static void setSessionId(String isUserLoggedIn, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("countryID", isUserLoggedIn);
        editor.commit();
    }

    public static String getPhoneNumber(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("phone", "");
    }

    public static void setPhoneNumber(String isUserLoggedIn, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("phone", isUserLoggedIn);
        editor.commit();
    }

    public static String getMarketData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("mar", "");
    }

    public static void setMarketData(String marketData, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("mar", marketData);
        editor.commit();
    }

    public static String getLocale(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("locale", "");
    }

    public static void setLocale(String locale, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("locale", locale);
        editor.commit();
    }

    public static JSONObject getRedefineCountries(JSONObject jsonObject) {

        JSONObject jsonObjectFinal = new JSONObject();

        Iterator<String> continentKeys = jsonObject.keys();
        while (continentKeys.hasNext()) {
            String continentKey = continentKeys.next();
            try {
                JSONObject objJson = jsonObject.getJSONObject(continentKey);
                {
                    JSONObject countries = objJson.getJSONObject("options");
                    Iterator<String> countrykeys = countries.keys();
                    while (countrykeys.hasNext()) {
                        String countryKey = countrykeys.next();
                        jsonObjectFinal.put(countryKey, countries.getString(countryKey));
                    }
                }
            } catch (Exception c) {
                return jsonObject;
            }
        }
        return jsonObjectFinal;
    }

    public static void sortTwoDifferentArraylists(ArrayList<String> mainList, ArrayList<String> list) {

        try {
            JSONObject object = new JSONObject();

            for (int i = 0; i < mainList.size(); i++) {
                object.put(mainList.get(i), list.get(i));
            }

            Collections.sort(mainList);
            list.clear();
            for (int i = 0; i < mainList.size(); i++) {
                list.add(object.getString(mainList.get(i)));
                //object.put(mainList.get(i), list.get(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
