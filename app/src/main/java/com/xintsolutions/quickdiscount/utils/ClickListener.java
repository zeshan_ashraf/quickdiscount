package com.xintsolutions.quickdiscount.utils;

import android.view.View;

/**
 * Created by Asad Waheed on 17-Mar-16.
 */
public  interface ClickListener {

    public  void onClick(View view, int position);
}
