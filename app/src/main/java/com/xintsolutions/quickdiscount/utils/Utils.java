package com.xintsolutions.quickdiscount.utils;

/**
 * Created by hp on 10/7/2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

public class Utils {

    private static final String TAG = "Utils";
    private static final String VERSION_UNAVAILABLE = "N/A";
    public static boolean isComingFromRedeem = false;
    public static final String PUSH_SENDER_ID = "702517026047";

    // ============================= For checking internet connectivity
    public static boolean isInternetOn(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // ARE WE CONNECTED TO THE NET
        if (connManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                || connManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
                || connManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
                || connManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        } else if (connManager.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                || connManager.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
            return false;
        }

        return false;
    }

    public static Drawable convertToGrayscale(Drawable drawable) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

        drawable.setColorFilter(filter);

        return drawable;
    }

    public static void setGrayScale(ImageView v, int saturation) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(saturation); //0 means grayscale
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        v.setColorFilter(cf);
    }

    public static String loadJSONFromAsset(String fileName, Activity activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static Bitmap getBitmapFromAsset(String strName, Context activity) {
        AssetManager assetManager = activity.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }

    public static String getAndroidId(Context currentActivity) {
        return Settings.Secure.getString(currentActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getVersionName(Context context) {
        // Get app version
        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        String versionName;
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = VERSION_UNAVAILABLE;
        }
        return versionName;
    }

    public static int getVersionCode(Context context) {
        // Get app version
        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        int versionCode;
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            versionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            versionCode = 0;
        }
        return versionCode;
    }

    public static void dumpIntent(Bundle bundle) {

        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                if (value != null)
                    Log.d(TAG, String.format("%s %s (%s)"
                            , key, value.toString()
                            , value.getClass().getName()));
                else
                    Log.d(TAG, String.format("%s value: NULL", key));
            }
        } else
            Log.d(TAG, "Bundle is EMPTY");
    }
}