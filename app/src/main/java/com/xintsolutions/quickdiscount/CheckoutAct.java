package com.xintsolutions.quickdiscount;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xintsolutions.quickdiscount.adapter.ListAdapterCheckout;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asad Waheed on 10/26/2016.
 */

public class CheckoutAct extends AppCompatActivity {

    AppCompatActivity mActivity;
    List<Product> listTips;
    ListView lvProduct;
    public static int subTotal;
    TextView tvSubTotalValue, tvHandlingChargesVal, tvTotalAmount;
    public static int handlingCharges = 100;
    public static int totalAmount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);
        handlingCharges = 100;
        subTotal = 0;
        totalAmount = 0;
        mActivity = this;
        lvProduct = (ListView) findViewById(R.id.lvProduct);
        tvSubTotalValue = (TextView) findViewById(R.id.tvSubTotalValue);
        tvHandlingChargesVal = (TextView) findViewById(R.id.tvHandlingChargesVal);
        tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
        findViewById(R.id.btnCheckOut).setOnClickListener(clickListener);
        if (listTips == null) {
            //////// new AsyncGetProducts().execute(id);
            listTips = Settings.getCartList(mActivity);
            if(listTips==null)
                listTips = new ArrayList<>();
            int size = listTips.size();
            for (int i = 0; i < size; i++) {
                Product product = listTips.get(i);
                subTotal = subTotal + product.priceOfOrder;
            }
            if (subTotal >= 5000) {
                handlingCharges = 0;
            } else {
                handlingCharges = 100;
            }
            tvSubTotalValue.setText(subTotal + "");
            tvHandlingChargesVal.setText(handlingCharges + "");
            totalAmount = subTotal + handlingCharges;
            tvTotalAmount.setText(totalAmount + "");
            lvProduct.setAdapter(new ListAdapterCheckout(
                    mActivity, R.layout.row_list_chkout, R.id.tvProdName, listTips,
                    tvSubTotalValue, tvHandlingChargesVal, tvTotalAmount));
        }
           /* lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Product product = (Product) lvProduct.getAdapter().getItem(i);
                    Intent intent = new Intent(mActivity, AddToCart.class);
                    intent.putExtra("product", product);
                    startActivity(intent);
                }
            });*/
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnCheckOut:
                    if(listTips.size()>0){
                    startActivity(new Intent(mActivity,UserInfoForm.class));
                    }else{
                        Toast.makeText(mActivity,"Your cart is empty!",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Settings.setCartList(mActivity, listTips);
    }

   // ProgressDialog mProgressDialog;

   /* private class AsyncGetProducts extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "category_products", "cat_name", urls[0]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        JSONArray msgObj = jsonObject.getJSONArray("category_products");
                        int size = msgObj.length();
                        listTips = new ArrayList<>();
                        Product cat;
                        JSONObject jObj;
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                cat = new Product();
                                jObj = msgObj.getJSONObject(i);
                                cat.id = jObj.getString("id");
                                cat.title = jObj.getString("title");
                                cat.image = jObj.getString("image");
                                cat.type = jObj.getString("type");
                                cat.price = jObj.getString("price");
                                cat.currency = jObj.getString("currency");
                                listTips.add(cat);
                            }
                        }
                        lvProduct.setAdapter(new ListAdapterProducts(
                                mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }*/

}
