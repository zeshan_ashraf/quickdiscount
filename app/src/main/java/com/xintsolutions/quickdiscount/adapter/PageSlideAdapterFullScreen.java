package com.xintsolutions.quickdiscount.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xintsolutions.quickdiscount.R;

import java.util.ArrayList;

/*import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;*/

public class PageSlideAdapterFullScreen extends PagerAdapter {
	Context activity;
	ArrayList<Integer> listIDs;
	//ImageLoader loader;
	

	public PageSlideAdapterFullScreen(Context searchActivity, ArrayList<Integer> arrayIDs) {
		this.activity = searchActivity;
		this.listIDs = arrayIDs;
		/*loader=ImageLoader.getInstance();
		loader.init(imageOptions(activity));*/
	}
	/*
private ImageLoaderConfiguration imageOptions(Context context){
	DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.person)
			.showImageOnLoading(new ColorDrawable(Color.parseColor("#000000")))
			.resetViewBeforeLoading(true)
			.showImageOnFail(R.drawable.person).resetViewBeforeLoading()
			.cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY)
			.cacheInMemory()
			.bitmapConfig(Bitmap.Config.RGB_565)
			.displayer(new FadeInBitmapDisplayer(300)).build();
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					context).threadPriority(Thread.NORM_PRIORITY - 1)
					.denyCacheImageMultipleSizesInMemory()
					.discCacheFileNameGenerator(new Md5FileNameGenerator())
					.defaultDisplayImageOptions(options)
					.memoryCacheSize(41943040)
					.discCacheSize(104857600)
					.threadPoolSize(10)
					.tasksProcessingOrder(QueueProcessingType.LIFO).build();
			return config;
}*/
	@Override
	public int getCount() {
		return listIDs.size();
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.vp_image_fullscreen, container, false);
		ImageView mImageView = (ImageView) view.findViewById(R.id.image_display);
		//loader.displayImage("drawable://"+listIDs.get(position), mImageView);
		Glide.with(activity).load(listIDs.get(position)).dontAnimate()
				.into(mImageView);

		/*if(s.startsWith("//")){
			s=s.replaceFirst("//", "http://");
		}*/
		 /*Glide.with(activity).load(Constants.getImageURL(s.user_id, s.image))
		 .placeholder(R.drawable.person_image).dontAnimate().error(R.drawable.person_image)
		 .into(mImageView);*/
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	
}