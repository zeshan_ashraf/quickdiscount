package com.xintsolutions.quickdiscount.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.reflect.TypeToken;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class ListAdapterProducts extends ArrayAdapter<Product> {
    Context mContext;
    ArrayList<Product> arrayPersons;
    ViewHolder holder;
    private List<Product> favList = new ArrayList<>();

    public ListAdapterProducts(Context context, int resource,
                               int textViewResourceId, ArrayList<Product> listFriends) {
        super(context, resource, textViewResourceId, listFriends);
        mContext = context;
        arrayPersons = listFriends;
        favList = Settings.getFavortList(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_list_frnds, parent, false);
            holder.tvName = (TextView) v.findViewById(R.id.tvProdName);
            holder.tvProdPrice = (TextView) v.findViewById(R.id.tvProdPrice);
            holder.ivImage = (ImageView) v.findViewById(R.id.ivProduct);
            holder.likeButton = (LikeButton) v.findViewById(R.id.star_button);
            holder.likeButton.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    Product p = (Product) likeButton.getTag();
                    favList.add(p);
                    Settings.setFavortList(mContext, favList);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    Product p = (Product) likeButton.getTag();
                    int favSize = favList.size();
                    for (int i = 0; i < favSize; i++) {
                        if (p.title.matches(favList.get(i).title)) {
                            favList.remove(i);
                            break;
                        }
                    }
                    Settings.setFavortList(mContext, favList);
                }
            });
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        Product person = arrayPersons.get(position);
        holder.tvName.setText(person.title);
        holder.tvProdPrice.setText(person.currency + ". " + person.price);
        Glide.with(mContext).load(person.image)
                .placeholder(new ColorDrawable(0xEEEEEE)).dontAnimate().error(new ColorDrawable(0xEEEEEE))
                .into(holder.ivImage);
        holder.likeButton.setTag(person);
        if (favList == null)
            favList = new ArrayList<>();
        int favSize = favList.size();
        for (int i = 0; i < favSize; i++) {
            if (person.title.matches(favList.get(i).title)) {
                holder.likeButton.setLiked(true);
                break;
            } else {
                holder.likeButton.setLiked(false);
            }
        }
        return v;
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return arrayPersons.get(position);
    }

    private class ViewHolder {

        public TextView tvProdPrice;
        public ImageView ivImage;
        public TextView tvName;
        public LikeButton likeButton;

    }
}
