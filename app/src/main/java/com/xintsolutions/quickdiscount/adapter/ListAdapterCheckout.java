package com.xintsolutions.quickdiscount.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xintsolutions.quickdiscount.CheckoutAct;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class ListAdapterCheckout extends ArrayAdapter<Product> {
    Context mContext;
    List<Product> arrayPersons;
    ViewHolder holder;
    TextView tvSubTotalValue, tvHandlingChargesVal, tvTotalAmount;
    ListAdapterCheckout adapter;

    public ListAdapterCheckout(Context context, int resource,
                               int textViewResourceId, List<Product> listFriends,
                               TextView tvSubTotalValue, TextView tvHandlingChargesVal,
                               TextView tvTotalAmount) {
        super(context, resource, textViewResourceId, listFriends);
        mContext = context;
        arrayPersons = listFriends;
        this.tvHandlingChargesVal = tvHandlingChargesVal;
        this.tvSubTotalValue = tvSubTotalValue;
        this.tvTotalAmount = tvTotalAmount;
        adapter = this;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_list_chkout, parent, false);
            holder.tvName = (TextView) v.findViewById(R.id.tvProdName);
            holder.tvProdPrice = (TextView) v.findViewById(R.id.tvProdPrice);
            holder.ivImage = (ImageView) v.findViewById(R.id.ivProduct);
            holder.ivCancel = (ImageView) v.findViewById(R.id.ivCancel);
            holder.decQuantity = (Button) v.findViewById(R.id.dec_quantity);
            holder.incQuantity = (Button) v.findViewById(R.id.inc_quantity);
            holder.quantity = (TextView) v.findViewById(R.id.quantity);
            holder.ivCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (int) view.getTag();
                    Product p = arrayPersons.get(position);
                    CheckoutAct.subTotal = CheckoutAct.subTotal - p.priceOfOrder;
                    if (CheckoutAct.subTotal >= 5000) {
                        CheckoutAct.handlingCharges = 0;
                    } else {
                        CheckoutAct.handlingCharges = 100;
                    }
                    tvSubTotalValue.setText(CheckoutAct.subTotal + "");
                    tvHandlingChargesVal.setText(CheckoutAct.handlingCharges + "");
                    CheckoutAct.totalAmount = CheckoutAct.subTotal + CheckoutAct.handlingCharges;
                    tvTotalAmount.setText(CheckoutAct.totalAmount + "");
                    arrayPersons.remove((int) position);
                    ListAdapterCheckout.this.notifyDataSetChanged();
                    // Settings.setCartList(mContext, arrayPersons);
                }
            });
            holder.decQuantity.setOnClickListener(clickListener);
            holder.incQuantity.setOnClickListener(clickListener);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        Product person = arrayPersons.get(position);
        holder.tvName.setText(person.title);
        holder.quantity.setText(person.quantity + "");
        holder.decQuantity.setTag(person);
        holder.incQuantity.setTag(person);
        holder.tvProdPrice.setText(person.currency + ". " + person.priceOfOrder + "\n(" + person.quantity + " X " +
                person.price + ")");
        Glide.with(mContext).load(person.image)
                .placeholder(new ColorDrawable(0xEEEEEE)).dontAnimate().error(new ColorDrawable(0xEEEEEE))
                .into(holder.ivImage);
        holder.ivCancel.setTag(position);
        return v;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.dec_quantity:
                    Product p = (Product) view.getTag();
                    if (p.quantity > 1) {
                        p.quantity--;
                        int price = (int) Double.parseDouble(p.price);
                        p.priceOfOrder = p.priceOfOrder - price;

                        adapter.notifyDataSetChanged();
                        updateValues();
                        //   arrayPersons.
                        //tvTotalAmount.setText(p.currency + ". " + ( price* p.quantity) + "");
                        //tvWeight.setText(p.quantity + "");
                    }
                    break;
                case R.id.inc_quantity:
                    p = (Product) view.getTag();
                    p.quantity++;
                    int price = (int) Double.parseDouble(p.price);
                    p.priceOfOrder = p.priceOfOrder + price;
                    adapter.notifyDataSetChanged();
                    updateValues();
                    break;

            }
        }
    };

    private void updateValues() {
        CheckoutAct.subTotal = 0;
        CheckoutAct.handlingCharges = 0;
        CheckoutAct.totalAmount = 0;
        int size = arrayPersons.size();
        for (int i = 0; i < size; i++) {
            Product product = arrayPersons.get(i);
            CheckoutAct.subTotal = CheckoutAct.subTotal + product.priceOfOrder;
        }
        if (CheckoutAct.subTotal >= 5000) {
            CheckoutAct.handlingCharges = 0;
        } else {
            CheckoutAct.handlingCharges = 100;
        }
        tvSubTotalValue.setText(CheckoutAct.subTotal + "");
        tvHandlingChargesVal.setText(CheckoutAct.handlingCharges + "");
        CheckoutAct.totalAmount = CheckoutAct.subTotal + CheckoutAct.handlingCharges;
        tvTotalAmount.setText(CheckoutAct.totalAmount + "");
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return arrayPersons.get(position);
    }

    private class ViewHolder {

        public TextView tvProdPrice;
        public ImageView ivImage;
        public ImageView ivCancel;
        public TextView tvName;

        public Button decQuantity;
        public Button incQuantity;
        public TextView quantity;
    }
}
