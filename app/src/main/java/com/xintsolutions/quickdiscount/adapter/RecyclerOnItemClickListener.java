package com.xintsolutions.quickdiscount.adapter;

import android.view.View;

public interface RecyclerOnItemClickListener {
    void onItemClicked(View view, int position);
}