package com.xintsolutions.quickdiscount.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.Category;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.ClickListener;

import java.util.ArrayList;

/**
 * {@link RecyclerView.Adapter} that can display a {@link //DummyItem} and makes a call to the
 * specified {@link ///OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapterThirdTab extends RecyclerView.Adapter<MyItemRecyclerViewAdapterThirdTab.ViewHolder>
        implements RecyclerOnItemClickListener {

    private final ArrayList<Category> mValues;
    private final ClickListener mListener;
    Context context;

    public MyItemRecyclerViewAdapterThirdTab(Context context, ArrayList<Category> items, ClickListener listener) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_third, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(holder.mItem.title);
        // holder.mContentView.setText(holder.mItem.price+" PKR");
        // holder.mDesc.setText(Html.fromHtml(holder.mItem.description));
        // holder.mDesc.setText(holder.mItem.description);
        if (!TextUtils.isEmpty(holder.mItem.image)) {
            // int id = context.getResources().getIdentifier(holder.mItem.image, "drawable", context.getPackageName());
            Glide.with(context).load(holder.mItem.image).placeholder(new ColorDrawable(0xEEEEEE))
                    .dontAnimate().into(holder.ivItem);
        } else {
            holder.ivItem.setImageResource(0);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onItemClicked(View view, int position) {
        mListener.onClick(view, position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final ImageView ivItem;
        public Category mItem;

        public ViewHolder(View view, final RecyclerOnItemClickListener recyclerOnItemClickListener) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            ivItem = (ImageView) view.findViewById(R.id.ivItem);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerOnItemClickListener != null) {
                        recyclerOnItemClickListener.onItemClicked(v, getAdapterPosition());
                    }
                }
            });
        }


    }

    public void addData(ArrayList<Category> data) {
        int sizeBeforeInsertion = mValues.size();
        mValues.addAll(data);
        notifyItemRangeInserted(sizeBeforeInsertion, data.size());
    }

    public Category getItem(int position) {
        return mValues.get(position);
    }
}
