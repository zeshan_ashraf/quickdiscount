package com.xintsolutions.quickdiscount.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.like.LikeButton;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.LineItems;
import com.xintsolutions.quickdiscount.model.Order;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class ListAdapterOrderItems extends ArrayAdapter<LineItems> {
    Context mContext;
    ArrayList<LineItems> arrayPersons;
    ViewHolder holder;
    private List<Product> favList = new ArrayList<>();

    public ListAdapterOrderItems(Context context, int resource,
                                 int textViewResourceId, ArrayList<LineItems> listFriends) {
        super(context, resource, textViewResourceId, listFriends);
        mContext = context;
        arrayPersons = listFriends;
        favList = Settings.getFavortList(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_list_items_in_order, parent, false);
            holder.tvItemName = (TextView) v.findViewById(R.id.tvItemName);
            holder.tvItemPrice = (TextView) v.findViewById(R.id.tvItemPrice);
            holder.tvItemQuantity = (TextView) v.findViewById(R.id.tvItemQuantity);
            holder.ivProduct = (ImageView) v.findViewById(R.id.ivProduct);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        LineItems person = arrayPersons.get(position);
        holder.tvItemName.setText(person.name);
        holder.tvItemPrice.setText(person.currency + " " + person.total);
        holder.tvItemQuantity.setText("Quantity " + person.quantity);
        Glide.with(mContext).load(person.product_img).placeholder(new ColorDrawable(0xF9D07F))
                .into(holder.ivProduct);
        return v;
    }

    @Nullable
    @Override
    public LineItems getItem(int position) {
        return arrayPersons.get(position);
    }

    private class ViewHolder {


        public ImageView ivProduct;
        public TextView tvItemQuantity;
        public TextView tvItemPrice;
        public TextView tvItemName;
    }
}
