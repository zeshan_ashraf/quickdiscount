package com.xintsolutions.quickdiscount.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.like.LikeButton;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.Order;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class ListAdapterOrders extends ArrayAdapter<Order> {
    Context mContext;
    ArrayList<Order> arrayPersons;
    ViewHolder holder;
    private List<Product> favList = new ArrayList<>();

    public ListAdapterOrders(Context context, int resource,
                             int textViewResourceId, ArrayList<Order> listFriends) {
        super(context, resource, textViewResourceId, listFriends);
        mContext = context;
        arrayPersons = listFriends;
        favList = Settings.getFavortList(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_list_orders, parent, false);
            holder.tvOrderNo = (TextView) v.findViewById(R.id.tvOrderNo);
            holder.tvOrderDate = (TextView) v.findViewById(R.id.tvOrderDate);
            holder.tvOrderPrice = (TextView) v.findViewById(R.id.tvOrderPrice);
            holder.ivOrderStatus = (ImageView) v.findViewById(R.id.ivOrderStatus);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        Order person = arrayPersons.get(position);
        holder.tvOrderNo.setText("Order No. "+person.id + "");
        holder.tvOrderDate.setText(person.date);
        holder.tvOrderPrice.setText(person.currency + ". " + person.total);
        if (person.status.equalsIgnoreCase("processing")) {
            Glide.with(mContext).load(R.mipmap.processing).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("pending")) {
            Glide.with(mContext).load(R.mipmap.pending).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("cancelled")) {
            Glide.with(mContext).load(R.mipmap.cancelled).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("completed")) {
            Glide.with(mContext).load(R.mipmap.compeleted).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("delivered")) {
            Glide.with(mContext).load(R.mipmap.delivered).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("failed")) {
            Glide.with(mContext).load(R.mipmap.failed).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("onhold")) {
            Glide.with(mContext).load(R.mipmap.onhold).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("order_accepted")) {
            Glide.with(mContext).load(R.mipmap.order_accept).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }else if (person.status.equalsIgnoreCase("refunded")) {
            Glide.with(mContext).load(R.mipmap.refunded).dontAnimate()
                    .into(holder.ivOrderStatus);
            //holder.ivOrderStatus.setImageResource(R.mipmap.processing);
        }


        return v;
    }

    @Nullable
    @Override
    public Order getItem(int position) {
        return arrayPersons.get(position);
    }

    private class ViewHolder {

        public TextView tvOrderPrice;
        public TextView tvOrderNo;
        public LikeButton likeButton;

        public TextView tvOrderDate;
        public ImageView ivOrderStatus;
    }
}
