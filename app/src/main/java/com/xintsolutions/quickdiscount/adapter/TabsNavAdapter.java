package com.xintsolutions.quickdiscount.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xintsolutions.quickdiscount.fragment.AllProductsFrg;
import com.xintsolutions.quickdiscount.fragment.ShopFragment;
import com.xintsolutions.quickdiscount.fragment.HomeFragment;
import com.xintsolutions.quickdiscount.fragment.OrdersFrg;


public class TabsNavAdapter extends FragmentPagerAdapter {
    Context mContext;
//    private final String[] TITLES;
    private final String[] TITLES = {"Home","Shop","Order","A-Z Products"};

public TabsNavAdapter(Activity myEventsSelector, FragmentManager mFragmentManager) {
    super(mFragmentManager);
    mContext = myEventsSelector;
//    TITLES = new String[]{mContext.getString(R.string.home), mContext.getString(R.string.favourit),
//            mContext.getString(R.string.setting), mContext.getString(R.string.me)};
}

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:

                return HomeFragment.newInstance(null, null);
            case 1:
                return ShopFragment.newInstance(null, null);
            case 2:
                return OrdersFrg.newInstance(null,null);
            case 3:
                return new AllProductsFrg();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }


}
