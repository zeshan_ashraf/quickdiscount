package com.xintsolutions.quickdiscount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;
import com.xintsolutions.quickdiscount.model.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hp on 10/3/2016.
 */

public class Signup extends AppCompatActivity {
    AppCompatActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        final EditText etFullName = (EditText) findViewById(R.id.etFullName);
        final EditText etEmail = (EditText) findViewById(R.id.etEmail);
        final EditText etPhone = (EditText) findViewById(R.id.etPhone);
        final EditText etAddress = (EditText) findViewById(R.id.etAddress);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final EditText etConfPaass = (EditText) findViewById(R.id.etConfPass);
        final EditText etFirstName = (EditText) findViewById(R.id.etFirstName);
        final EditText etLastName = (EditText) findViewById(R.id.etLastName);
        Button btnSignUp = (Button) findViewById(R.id.etSignUp);
        mActivity = this;
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullName = etFullName.getText().toString();
                String phone = etPhone.getText().toString();
                String email = etEmail.getText().toString();
                String address = etAddress.getText().toString();
                String password = etPassword.getText().toString();
                String confPass = etConfPaass.getText().toString();
                String fname = etFirstName.getText().toString();
                String lname = etLastName.getText().toString();
                boolean cancel = false;
                if (TextUtils.isEmpty(fullName) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email) ||
                        TextUtils.isEmpty(address) || TextUtils.isEmpty(password) ||
                        TextUtils.isEmpty(confPass) || TextUtils.isEmpty(fname) || TextUtils.isEmpty(lname)) {
                    Toast.makeText(mActivity, "A required field is missing.", Toast.LENGTH_SHORT)
                            .show();
                    cancel = true;

                }
                if (!password.equals(confPass)) {
                    cancel = true;
                }
                if (!cancel) {
                    new AsyncSignUp().execute(fullName, phone, email, address, password, fname, lname);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Signup.this, MainActivity.class);
        startActivity(intent);
    }

    ProgressDialog mProgressDialog;

    private class AsyncSignUp extends AsyncTask<String, Long, String> {
        public String phone;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                phone = urls[0];
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "register_user", "user_name", urls[0], "phone_number", urls[1],
                        "email", urls[2], "address", urls[3], "password", urls[4], "first_name", urls[5],
                        "last_name", urls[6]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        Settings.setIsUserLoggedIn(true, mActivity);
                        JSONObject msgObj = jsonObject.getJSONObject("user");
                        User user = new User();
                        user.user_id = msgObj.getString("user_id");
                        user.email_address = msgObj.getString("email_address");
                        user.user_name = msgObj.getString("user_name");
                        user.address = msgObj.getString("address");
                        user.phone_number = msgObj.getString("phone_number");
                        user.first_name = msgObj.getString("first_name");
                        user.last_name = msgObj.getString("last_name");
                        Settings.setUserId(user.user_id, mActivity);
                        Settings.setUser(mActivity, user);
                        startActivity(new Intent(mActivity, DrawerActivity.class));
                        finish();
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
