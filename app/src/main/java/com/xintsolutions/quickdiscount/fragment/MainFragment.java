package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.adapter.TabsNavAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    AppCompatActivity mActivity;
    private OnFragmentInteractionListener mListener;
    private SmartTabLayout tabs;
    private ViewPager pager;
    private TabsNavAdapter adapter;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.main_screen, container, false);
//        initUI(v);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else if (context instanceof Activity) {
            mActivity = (AppCompatActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

//    private void initUI(View view) {
//        tabs = (SmartTabLayout) view.findViewById(R.id.tabs);
//        pager = (ViewPager) view.findViewById(R.id.pager);
//        tabs.setDistributeEvenly(true);
//        tabs.setCustomTabView(new SmartTabLayout.TabProvider() {
//            @Override
//            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
//                LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View tabView = inflater.inflate(R.layout.layout, container, false);
//                ImageView ivTabImage = (ImageView) tabView.findViewById(R.id.ivTab);
//                TextView tvTab = (TextView) tabView.findViewById(R.id.tvTab);
//               /* if(tabView.gets)){
//                    tvTab.setTextColor(ContextCompat.getColor(mActivity,R.color.tabs_text_color));
//                }*/
//                tvTab.setText(adapter.getPageTitle(position));
//                switch (position) {
//                    case 0:
//                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.favourit));
//                        break;
//                    case 1:
//                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.favourit));
//                        break;
//                    case 2:
//                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.favourit));
//                        break;
//                    case 3:
//                        ivTabImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.favourit));
//                        break;
//
//                    default:
//                        throw new IllegalStateException("Invalid position: " + position);
//                }
//                return tabView;
//            }
//        });
//        adapter = new TabsNavAdapter(mActivity, getChildFragmentManager());
//        pager.setAdapter(adapter);
//        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
//                .getDisplayMetrics());
//        pager.setPageMargin(pageMargin);
//        tabs.setViewPager(pager);
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
