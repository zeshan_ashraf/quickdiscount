package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.OrderItemsAct;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.adapter.ListAdapterOrders;
import com.xintsolutions.quickdiscount.adapter.MyItemRecyclerViewAdapterThirdTab;
import com.xintsolutions.quickdiscount.model.LineItems;
import com.xintsolutions.quickdiscount.model.Order;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class OrdersFrg extends Fragment {

    DrawerActivity mActivity;
    public static ArrayList<Order> listTips;
    ListView lvOrders;

    public static OrdersFrg newInstance(String term, String title) {
        OrdersFrg fragment = new OrdersFrg();
        Bundle b = new Bundle();
        b.putString("term", term);
        b.putString("title", title);
        fragment.setArguments(b);
        return fragment;
    }

    public OrdersFrg() {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity)
            mActivity = (DrawerActivity) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.workout_invites, container, false);
        lvOrders = (ListView) rootView.findViewById(R.id.listOrderes);
        /*if(mActivity==null){
            mActivity = (DrawerActivity) getActivity();
        }*/
        lvOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Order order = listTips.get(i);
                if (order.line_items.size() > 0) {
                    Intent intent = new Intent(mActivity, OrderItemsAct.class);
                    intent.putExtra("order", order.line_items);
                    startActivity(intent);
                } else {
                    Toast.makeText(mActivity, "This order has no products.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (listTips == null) {
                if (mActivity != null) {
                    new AsyncGetOrders().execute();
                }
            } else {
                if (mActivity != null) {
                    lvOrders.setAdapter(new ListAdapterOrders(
                            mActivity, R.layout.row_list_orders, R.id.tvProdName, listTips));
                }
            }
        }
    }

    ProgressDialog mProgressDialog;

    private class AsyncGetOrders extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {

                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "list_orders", "customer_id", Settings.getUserId(mActivity));
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    // JSONObject jsonObject = new JSONObject(result);
                    // if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                    JSONArray msgObj = new JSONArray(result);
                    int size = msgObj.length();
                    listTips = new ArrayList<>();
                    Order cat;
                    JSONObject jObj;
                    JSONObject linObj;
                    if (size > 0) {
                        for (int i = 0; i < size; i++) {
                            cat = new Order();
                            jObj = msgObj.getJSONObject(i);
                            cat.id = jObj.getInt("id");
                            cat.date = jObj.getString("date_created");
                            cat.status = jObj.getString("status");
                            String[] dateArr = cat.date.split("T");
                            cat.date = dateArr[0];
                            cat.currency = jObj.getString("currency");
                            cat.total = jObj.getString("total");
                            JSONArray arrLin = jObj.getJSONArray("line_items");
                            int arSize = arrLin.length();
                            cat.line_items = new ArrayList<>();
                            LineItems items;
                            if (arSize > 0) {
                                for (int j = 0; j < arSize; j++) {
                                    items = new LineItems();
                                    linObj = arrLin.getJSONObject(j);
                                    items.name = linObj.getString("name");
                                    items.id = linObj.getString("id");
                                    items.total = linObj.getString("total");
                                    items.price = linObj.getString("price");
                                    items.product_img = linObj.getString("product_img");
                                    items.quantity = linObj.getString("quantity");
                                    items.currency = cat.currency;
                                    cat.line_items.add(items);
                                }
                            }
                            //  cat.currency = jObj.getString("currency");
                            listTips.add(cat);
                        }
                        lvOrders.setAdapter(new ListAdapterOrders(
                                mActivity, R.layout.row_list_orders, R.id.tvProdName, listTips));
                    }

                    // } else {
                    //     Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    // }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}

