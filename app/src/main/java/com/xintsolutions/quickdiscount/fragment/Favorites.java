package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xintsolutions.quickdiscount.AddToCart;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.adapter.ListAdapterFvrts;
import com.xintsolutions.quickdiscount.adapter.ListAdapterProducts;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;

/**
 * Created by hp on 10/26/2016.
 */

public class Favorites extends Fragment {

    DrawerActivity mActivity;
    ArrayList<Product> listTips;
    public static int REP_DELAY = 50;
    private Handler repeatUpdateHandler = new Handler();
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;
    private boolean mAutoMaxIncrement = false;
    private boolean mAutoMaxDecrement = false;
    ListView lvProduct;
    TextView minprice, maxPrice;
    int price = 1;
    int priceMax = 100;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.product,container,false);
        lvProduct = (ListView) v.findViewById(R.id.lvProduct);
        v.findViewById(R.id.min_dec_price).setOnClickListener(clickListener);
        v.findViewById(R.id.min_inc_price).setOnClickListener(clickListener);
        v.findViewById(R.id.max_dec_price).setOnClickListener(clickListener);
        v.findViewById(R.id.max_inc_price).setOnClickListener(clickListener);
        v.findViewById(R.id.max_inc_price).setOnLongClickListener(onLongClickListener);
        v.findViewById(R.id.max_dec_price).setOnLongClickListener(onLongClickListener);
        v.findViewById(R.id.min_inc_price).setOnLongClickListener(onLongClickListener);
        v.findViewById(R.id.min_dec_price).setOnLongClickListener(onLongClickListener);
        v.findViewById(R.id.max_inc_price).setOnTouchListener(onTouchListener);
        v.findViewById(R.id.max_dec_price).setOnTouchListener(onTouchListener);
        v.findViewById(R.id.min_inc_price).setOnTouchListener(onTouchListener);
        v.findViewById(R.id.min_dec_price).setOnTouchListener(onTouchListener);
        v.findViewById(R.id.btnApply).setOnClickListener(clickListener);
        minprice = (TextView) v.findViewById(R.id.minprice);
        maxPrice = (TextView) v.findViewById(R.id.maxPrice);

            listTips= (ArrayList<Product>) Settings.getFavortList(mActivity);
            if(listTips!=null){
                lvProduct.setAdapter(new ListAdapterFvrts(
                        mActivity, R.layout.row_list_frnds, R.id.tvProdName, listTips));
            }
            lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Product product = (Product) lvProduct.getAdapter().getItem(i);
                    Intent intent = new Intent(mActivity, AddToCart.class);
                    intent.putExtra("product", product);
                    startActivity(intent);
                }
            });
        mActivity.tvTitle.setText("Favorites");
        return v;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.min_dec_price:
                    if (price > 1) {
                        price--;
                        minprice.setText(price + "");
                    }
                    break;
                case R.id.min_inc_price:
                    price++;
                    minprice.setText(price + "");
                    break;
                case R.id.max_dec_price:
                    if (priceMax > 1) {
                        priceMax--;
                        maxPrice.setText(priceMax + "");
                    }
                    break;
                case R.id.max_inc_price:
                    priceMax++;
                    maxPrice.setText(priceMax + "");
                    break;
                case R.id.btnApply:
                    ArrayList listFiltered = new ArrayList();
                    int loopPrice;
                    if (listTips != null) {
                        for (int i = 0; i < listTips.size(); i++) {
                            loopPrice = Math.round(Float.parseFloat(listTips.get(i).price));
                            if (loopPrice >= price && loopPrice <= priceMax) {
                                listFiltered.add(listTips.get(i));
                            }
                        }
                        if (listFiltered.size() > 0) {
                            lvProduct.setAdapter(new ListAdapterProducts(
                                    mActivity, R.layout.row_list_frnds, R.id.tvProdName, listFiltered));
                        } else {
                            Toast.makeText(mActivity, "Your selected filters returned no matches."
                                    , Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
            }
        }
    };
    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            switch (view.getId()) {
                case R.id.min_dec_price:
                    mAutoDecrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.min_inc_price:
                    mAutoIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.max_dec_price:
                    mAutoMaxDecrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
                case R.id.max_inc_price:
                    mAutoMaxIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    break;
            }
            return false;
        }
    };
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                //&& mAutoIncrement
                    ) {
                mAutoIncrement = false;
                mAutoMaxDecrement = false;
                mAutoMaxIncrement = false;
                mAutoDecrement = false;
            }
            return false;
        }
    };

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return false;
    }*/


    class RptUpdater implements Runnable {
        public void run() {
            if (mAutoIncrement) {
                incrementMin();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoDecrement) {
                decrementMin();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoMaxIncrement) {
                incrementMax();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoMaxDecrement) {
                decrementMax();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            }
        }
    }

    public void decrementMin() {
        if (price > 1) {
            price--;
            minprice.setText(price + "");
        }
    }

    public void incrementMin() {
        price++;
        minprice.setText(price + "");
    }

    public void decrementMax() {
        if (priceMax > 1) {
            priceMax--;
            maxPrice.setText(priceMax + "");
        }
    }

    public void incrementMax() {
        priceMax++;
        maxPrice.setText(priceMax + "");
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity) {
            mActivity = (DrawerActivity) context;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }
}
