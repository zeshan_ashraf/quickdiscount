package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.AutoScrollHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ToxicBakery.viewpager.transforms.AccordionTransformer;
import com.ToxicBakery.viewpager.transforms.CubeInTransformer;
import com.ToxicBakery.viewpager.transforms.FlipVerticalTransformer;
import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.xintsolutions.quickdiscount.Login;
import com.xintsolutions.quickdiscount.MainActivity;
import com.xintsolutions.quickdiscount.MainScreen;
import com.xintsolutions.quickdiscount.Products;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.Signup;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.activity.WebMapActivity;
import com.xintsolutions.quickdiscount.adapter.PageSlideAdapterFullScreen;
import com.xintsolutions.quickdiscount.model.Category;
import com.xintsolutions.quickdiscount.utils.CirclePageIndicator;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.PageIndicator;
import com.xintsolutions.quickdiscount.utils.Settings;

import java.util.ArrayList;

//import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class HomeFragment extends Fragment implements EasyVideoCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    ArrayList<Integer> list;
    DrawerActivity mActivity;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    MainScreen parentFrg;
    private EasyVideoPlayer player;
    boolean mute = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        // mViewPager = (AutoScrollViewPager) v.findViewById(R.id.view_pager);
        // mIndicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
        parentFrg = (MainScreen) getParentFragment();
        // Grabs a reference to the player view
        player = (EasyVideoPlayer) v.findViewById(R.id.player);

        // Sets the callback to this Activity, since it inherits EasyVideoCallback
        player.setCallback(this);
        player.setLoop(true);
        player.disableControls();
        player.setAutoPlay(true);
        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        try {
            player.setSource(Uri.parse(Constants.VIDEO_URL));
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
        list = new ArrayList<>();
        list.add(R.mipmap.slider01);
        list.add(R.mipmap.slider02);
        list.add(R.mipmap.slider03);
        v.findViewById(R.id.btnProfile).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnSettings).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnNotif).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnFavrt).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnHotelBookings).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnCinemaBooking).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnPerfume).setOnClickListener(onClickListener);
        v.findViewById(R.id.btnShoes).setOnClickListener(onClickListener);
        if (Settings.getIsUserLoggedIn(mActivity)) {
            v.findViewById(R.id.rlStarting).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.tvSignIn).setOnClickListener(onClickListener);
            v.findViewById(R.id.tvCreateAc).setOnClickListener(onClickListener);
        }
        player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player.isPrepared()) {
                    if (mute)
                        player.setVolume(1, 1);
                    else
                        player.setVolume(0, 0);
                    mute = !mute;
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (player.isPrepared()) {
            player.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvSignIn:
                    Intent intent = new Intent(mActivity, Login.class);
                    startActivity(intent);
                    break;
                case R.id.tvCreateAc:
                    intent = new Intent(mActivity, Signup.class);
                    startActivity(intent);
                    break;
                case R.id.btnSettings:
                    intent = new Intent(mActivity, Products.class);
                    intent.putExtra("id", "240");
                    intent.putExtra("title", "Laundry");
                    startActivity(intent);
                    break;
                case R.id.btnProfile:
                    parentFrg.pager.setCurrentItem(1);
                    break;
                case R.id.btnFavrt:
                    String posted_by = "111-333-222-4";
                    String uri = "tel:" + posted_by.trim();
                    intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                    break;
                case R.id.btnNotif:
                    intent = new Intent(mActivity, Products.class);
                    intent.putExtra("id", "297");
                    intent.putExtra("title", "Pharmacy");
                    startActivity(intent);
                    break;
                case R.id.btnHotelBookings:
                    intent = new Intent(mActivity, WebMapActivity.class);
                    startActivity(intent);
                    break;
                case R.id.btnCinemaBooking:
                    intent = new Intent(mActivity, Products.class);
                    intent.putExtra("id", "300");
                    intent.putExtra("title", "Cinema Bookings");
                    startActivity(intent);
                    break;
                case R.id.btnPerfume:
                    intent = new Intent(mActivity, Products.class);
                    intent.putExtra("id", "298");
                    intent.putExtra("title", "Perfume");
                    startActivity(intent);
                    break;
                case R.id.btnShoes:
                    intent = new Intent(mActivity, Products.class);
                    intent.putExtra("id", "299");
                    intent.putExtra("title", "Shoes");
                    startActivity(intent);
                    break;
            }
        }
    };

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {
        player.setVolume(0, 0);
    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {
        private int currentPage;

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {/*
                if (products != null) {
					imgNameTxt.setText(""
							+ ((Product) products.get(mViewPager
									.getCurrentItem())).getName());
				}
			 */
                //Toast.makeText(getActivity(), "something", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        public final int getCurrentPage() {
            return currentPage;
        }

        @Override
        public void onPageSelected(int arg0) {
            currentPage = arg0;

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity) {
            mActivity = (DrawerActivity) context;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }


}
