package com.xintsolutions.quickdiscount.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xintsolutions.quickdiscount.MainActivity;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.utils.Settings;

public class MeFrg extends Fragment {
    Button my_photos, setting, contacts;
    ImageView url_img_me, img_qrcode;
    TextView url_txt_me, btnLogOUt;
    DrawerActivity mActivity;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_me, container, false);
        //below QR Code
        mActivity = (DrawerActivity) getActivity();
//        mActivity.etURL.setVisibility(View.GONE);
        //my_photos = (Button) view.findViewById(R.id.my_photos);
        //setting = (Button) view.findViewById(R.id.settings);
        //contacts = (Button) view.findViewById(R.id.friends);
        url_img_me = (ImageView) view.findViewById(R.id.img_me);
        url_txt_me = (TextView) view.findViewById(R.id.txt_me);
        btnLogOUt = (TextView) view.findViewById(R.id.btnLogOUt);
        btnLogOUt.setOnClickListener(listener);
        User user = Settings.getUser(mActivity);
        url_txt_me.setText(user.user_name);
        return view;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnLogOUt:
                    Settings.setIsUserLoggedIn(false, mActivity);
                    Intent intent = new Intent(mActivity, MainActivity.class);
                    startActivity(intent);
                    mActivity.finish();
                    break;
            }
        }


    };


}
