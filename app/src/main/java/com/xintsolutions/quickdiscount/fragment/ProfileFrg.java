package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.Signup;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.model.UserInfo;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.InternalStorageContentProvider;
import com.xintsolutions.quickdiscount.utils.Settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Asad Waheed on 10/9/2016.
 */

public class ProfileFrg extends Fragment {

    AppCompatActivity mActivity;
    TextView tvEdit;
    EditText etPhone, etAddress, etFName, etLName;
    public static String TEMP_PHOTO_FILE_NAME;
    final static int MEDIA_TYPE_IMAGE_BY_CAMERA = 61;
    final static int MEDIA_TYPE_IMAGE_FROM_GALLERY = 62;
    File file;
    String picturePath;
    String photoPath;
    ImageView ivPic;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_profile, container, false);
        ivPic = (ImageView) v.findViewById(R.id.imageView2);
        EditText etUserName = (EditText) v.findViewById(R.id.etUserName);
        EditText etEmail = (EditText) v.findViewById(R.id.etEmail);
        etPhone = (EditText) v.findViewById(R.id.etPhone);
        etAddress = (EditText) v.findViewById(R.id.etAddress);
        etFName = (EditText) v.findViewById(R.id.etFName);
        etLName = (EditText) v.findViewById(R.id.etLName);
        tvEdit = (TextView) v.findViewById(R.id.tvEdit);
        tvEdit.setOnClickListener(clickListener);
        ivPic.setOnClickListener(clickListener);
        User user = Settings.getUser(mActivity);
        etUserName.setText(user.user_name);
        etEmail.setText(user.email_address);
        etPhone.setText(user.phone_number);
        etAddress.setText(user.address);
        etFName.setText(user.first_name);
        etLName.setText(user.last_name);
        if(user.image!=null)
        Glide.with(mActivity).load(user.image).placeholder(new ColorDrawable(0xEDEDED)).into(ivPic);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity) {
            mActivity = (DrawerActivity) context;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View vie) {
            switch (vie.getId()) {
                case R.id.imageView2:
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
                    alertDialog.setTitle("Change Profile Picture");
                    LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.dialog_add_phto, null);
                    Button btnOpenGallery = (Button) view.findViewById(R.id.btnGallery);
                    Button btnCamera = (Button) view.findViewById(R.id.btnCamera);
                    alertDialog.setView(view);
                    final AlertDialog alertiDialog = alertDialog.create();
                    btnCamera.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            takePicture();
                            alertiDialog.dismiss();
                        }
                    });
                    btnOpenGallery.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            openGallery();
                            alertiDialog.dismiss();
                        }
                    });
                    alertiDialog.show();
                    break;
                case R.id.tvEdit:
                    if (tvEdit.getText().toString().equalsIgnoreCase("Edit")) {
                        tvEdit.setText("Save");
                        etFName.setEnabled(true);
                        etLName.setEnabled(true);
                        etPhone.setEnabled(true);
                        etAddress.setEnabled(true);
                    } else {
                        String phone = etPhone.getText().toString();
                        String address = etAddress.getText().toString();
                        String fname = etFName.getText().toString();
                        String lname = etLName.getText().toString();
                        boolean cancel = false;
                        if (TextUtils.isEmpty(phone) ||
                                TextUtils.isEmpty(address) ||
                                TextUtils.isEmpty(fname) || TextUtils.isEmpty(lname)) {
                            Toast.makeText(mActivity, "A required field is missing.", Toast.LENGTH_SHORT)
                                    .show();
                            cancel = true;

                        }
                        if (!cancel) {
                            new AsyncUpdateProfile().execute(phone, address, fname, lname);
                        }
                    }
                    break;

            }
        }
    };


    ProgressDialog mProgressDialog;

    private class AsyncUpdateProfile extends AsyncTask<String, Long, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "update_user_profile", "user_id", Settings.getUserId(mActivity), "phone_number",
                        urls[0], "address", urls[1], "first_name", urls[2], "last_name", urls[3]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        Toast.makeText(mActivity, "Profile Updated.", Toast.LENGTH_SHORT).show();
                        etFName.setEnabled(false);
                        etLName.setEnabled(false);
                        etAddress.setEnabled(false);
                        etPhone.setEnabled(false);
                        tvEdit.setText("Edit");
                        User user = new User();
                        JSONObject msgObj = jsonObject.getJSONObject("user");
                        user.user_id = msgObj.getString("user_id");
                        user.email_address = msgObj.getString("email_address");
                        user.user_name = msgObj.getString("user_name");
                        user.address = msgObj.getString("address");
                        user.phone_number = msgObj.getString("phone_number");
                        user.first_name = msgObj.getString("first_name");
                        user.last_name = msgObj.getString("last_name");
                        Settings.setUser(mActivity, user);
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class UpdatePhotos extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null)
                mProgressDialog = new ProgressDialog(mActivity);
         /*   pDialog.setMessage("Uploading...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);*/
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpRequest request = HttpRequest.post(Constants.BASE_URL+"?action=qdiscount_user_image_update",
                        true
                       // , "user_id", params[0]
                );
                String filePath = params[0];
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(filePath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out); // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.flush();
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                String mimeType;
                if (filePath.endsWith(".jpg") || filePath.endsWith(".jpeg")) {
                    mimeType = "image/jpg";
                } else {
                    mimeType = "image/png";
                }
                request.part("img_name", System.currentTimeMillis() + ".jpg", mimeType, new File(params[0]));
                request.part("user_id", Settings.getUserId(mActivity));
                bitmap.recycle();
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss(); //// Dismiss dialog
            if (result != null) {
                if (result.contains("out of memory")) {
                    Toast.makeText(mActivity, "Your phone ran out of memory while uploading.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject obj = new JSONObject(result);

                        String error = obj.getString("error");
                        if (error.equalsIgnoreCase("0")) {
                            Glide.with(mActivity).load(obj.getString("image_path")).placeholder(
                                    new ColorDrawable(0xEEDDEE)).into(ivPic);
                            User user = Settings.getUser(mActivity);
                            user.image = obj.getString("image_path");
                            Settings.setUser(mActivity, user);
                            //listCategories.add("add");
                           /* Image image = new Image();
                            image.photo = Constants.URL_PHOTOS + obj.getString("photo");
                            Glide.with(mActivity).load(image.photo).placeholder(new ColorDrawable(0xED1B24))
                                    .into(ivUserImg);
                            adapter.add(image, 0);*/
                            /*int size = listCategories.size();
                            if (size < 6) {
                                int tem = 6 - size;
                                for (int i = tem; i > 1; i--) {
                                    listCategories.add("add_photo");
                                }
                            }*/


                            //   new GetPhotos().execute();
                            //      Toast.makeText(mActivity, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            ;
                        } else {
                            Toast.makeText(mActivity, obj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Toast.makeText(mActivity, "Server not reachable or internet not available.", Toast.LENGTH_SHORT).show();
            }


        }

    }

    private void takePicture() {
        TEMP_PHOTO_FILE_NAME = System.currentTimeMillis() + ".jpeg";
        //file = new File(dir,strFileName);
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            String dir = Environment.getExternalStorageDirectory() + "/fyt/Images/";
            File temp = new File(dir);
            boolean cehk = temp.mkdirs();
            file = new File(Environment.getExternalStorageDirectory() + "/fyt/Images/", TEMP_PHOTO_FILE_NAME);
        } else {
            file = new File(mActivity.getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        photoPath = file.getAbsolutePath();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(file);
            } else {
                // The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder

                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, MEDIA_TYPE_IMAGE_BY_CAMERA);

        } catch (ActivityNotFoundException e) {

            Log.d("ExListAdap", "cannot take picture", e);
        }
    }


    private void openGallery() {
        String TEMP_PHOTO_FILE_NAME = System.currentTimeMillis() + ".jpeg";
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {

            String dir = Environment.getExternalStorageDirectory() + "/fyt/Images/";
            File temp = new File(dir);
            boolean cehk = temp.mkdirs();
            file = new File(Environment.getExternalStorageDirectory() + "/fyt/Images/", TEMP_PHOTO_FILE_NAME);
        } else {
            file = new File(mActivity.getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        photoPath = file.getAbsolutePath();
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, MEDIA_TYPE_IMAGE_FROM_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = mActivity.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            this.picturePath = picturePath;
            //ExifUtil.rotateBitmap(picturePath, BitmapFactory.decodeFile(picturePath));
            new UpdatePhotos().execute( this.picturePath);


        } else if (requestCode == MEDIA_TYPE_IMAGE_BY_CAMERA && resultCode == Activity.RESULT_OK) {

            File f = new File(photoPath);
            String picturePath = f.getPath();
            this.picturePath = picturePath;
            if (this.picturePath != null) {
                //ExifUtil.rotateBitmap(picturePath, BitmapFactory.decodeFile(picturePath));
                new UpdatePhotos().execute(Settings.getUserId(mActivity), this.picturePath);

            }

        }
        Log.d("MyAdapter", "onActivityResult");

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("photo", photoPath);
        outState.putString("pict", picturePath);
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            picturePath = savedInstanceState.getString("photo");
            photoPath = savedInstanceState.getString("photo");
        }
    }

}
