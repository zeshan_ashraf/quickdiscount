package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;


import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.Products;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.adapter.MyItemRecyclerViewAdapterThirdTab;
import com.xintsolutions.quickdiscount.model.Category;
import com.xintsolutions.quickdiscount.utils.ClickListener;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.ItemDecorationAlbumColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ShopFragment extends Fragment {

    DrawerActivity mActivity;
    ArrayList<Category> listTips;
    public SearchView etURL;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShopFragment() {
    }

    @SuppressWarnings("unused")
    public static ShopFragment newInstance(String term, String title) {
        ShopFragment fragment = new ShopFragment();
        Bundle b = new Bundle();
        b.putString("term", term);
        b.putString("title", title);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        listTipsFiltered = new ArrayList<>();
    }

    int textlength;
    ArrayList<Category> listTipsFiltered;
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        //llFilters = (LinearLayout) view.findViewById(R.id.filter);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 2));
        recyclerView.addItemDecoration(new ItemDecorationAlbumColumns(25, 2));
        etURL = (SearchView) view.findViewById(R.id.etURL);
        mActivity.tvTitle.setText("Quick Discount");
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) etURL.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        etURL.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    etURL.requestFocus();
                } else {
                    etURL.clearFocus();
                }
            }
        });
        recyclerView.requestFocus();
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                recyclerView.requestFocus();
                return false;
            }
        });
        etURL.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(mActivity!=null && listTips !=null) {
                    textlength = etURL.getQuery().length();
                    String searchInput = etURL.getQuery().toString();
                    if (textlength > 0) {
                        if (!listTips.isEmpty()) {
                            listTipsFiltered.clear();
                            for (int i = 0; i < listTips.size(); i++) {
                                if (listTips.get(i).title.toLowerCase().contains(
                                        searchInput.toLowerCase())) {
                                    listTipsFiltered.add(listTips.get(i));
                                }
                            }
                        }

                        recyclerView.setAdapter(new MyItemRecyclerViewAdapterThirdTab(
                                mActivity, listTipsFiltered, listener));
                        return true;
                    } else {
                        if (mActivity != null)
                            recyclerView.setAdapter(new MyItemRecyclerViewAdapterThirdTab(
                                    mActivity, listTips, listener));
                        return true;
                    }
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (listTips == null && mActivity!=null) {
                new AsyncGetCategories().execute();
            } else {
                if(mActivity!=null)
                recyclerView.setAdapter(new MyItemRecyclerViewAdapterThirdTab(
                        mActivity, listTips, listener));
            }
        }
    }
/*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.category_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
                if (llFilters.getVisibility() == View.GONE)
                    llFilters.setVisibility(View.VISIBLE);
                else
                    llFilters.setVisibility(View.GONE);
                break;
            case R.id.menu_cart:
                break;
        }
        return false;
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity) {
            mActivity = (DrawerActivity) context;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }


    ClickListener listener = new ClickListener() {
        @Override
        public void onClick(View view, int position) {
            Category cat = ((MyItemRecyclerViewAdapterThirdTab) recyclerView.getAdapter()).getItem(position);
            Intent intent = new Intent(mActivity, Products.class);
            intent.putExtra("id", cat.id);
            intent.putExtra("title", cat.title);
            startActivity(intent);
          /*  Product product =listTips.get(position);
            ProdDetScreen screen = new ProdDetScreen();
            Bundle bundle = new Bundle();
            bundle.putSerializable("data",product);
            screen.setArguments(bundle);
            mActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_containers,screen)
                    .addToBackStack(null).commit();*/
        }
    };
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


    ProgressDialog mProgressDialog;

    private class AsyncGetCategories extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "get_all_categories");
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        JSONArray msgObj = jsonObject.getJSONArray("categories");
                        int size = msgObj.length();
                        listTips = new ArrayList<>();
                        Category cat;
                        JSONObject jObj;
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                cat = new Category();
                                jObj = msgObj.getJSONObject(i);
                                cat.id = jObj.getString("id");
                                cat.title = Html.fromHtml(jObj.getString("title")).toString();
                                cat.image = jObj.getString("image");
                                if (!((cat.id.equalsIgnoreCase("240")) || (cat.id.equalsIgnoreCase("240")) ||
                                        (cat.id.equalsIgnoreCase("297")) || (cat.id.equalsIgnoreCase("298")) ||
                                        (cat.id.equalsIgnoreCase("299")) || (cat.id.equalsIgnoreCase("300"))))
                                    listTips.add(cat);
                            }
                        }
                        if(mActivity!=null)
                        recyclerView.setAdapter(new MyItemRecyclerViewAdapterThirdTab(
                                mActivity, listTips, listener));

                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
