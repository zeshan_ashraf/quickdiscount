package com.xintsolutions.quickdiscount.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.activity.DrawerActivity;
import com.xintsolutions.quickdiscount.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class MapFrg extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    SupportMapFragment fragment;
    AppCompatActivity mActivity;
   /* private final long MIN_TIME = 400;
    private final float MIN_DISTANCE = 1000;
    private LocationManager locationManager;
    private boolean mShowGpsOnDialog = true;
    GoogleApiClient mGoogleApiClient;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    Location loc;*/

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int hasFineLocation = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasFineLocation == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
            } catch (Exception e) {

            }
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


    }*/


    String orderId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_maps, container, false);
        fragment = SupportMapFragment.newInstance();
        fragment.getMapAsync(this);
        getChildFragmentManager()
                .beginTransaction().add(R.id.map_container, fragment).commit();
        if (OrdersFrg.listTips != null)
            if (OrdersFrg.listTips.size() > 0) {
                orderId = OrdersFrg.listTips.get(0).id + "";
            }

        return v;
    }

   /* public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == Constants.REQUEST_CHECK_SETTINGS)) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getLocation();
                    break;

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DrawerActivity) {
            mActivity = (DrawerActivity) context;
        }
        if (h != null) {
            h.postDelayed(finalizer, delay);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (DrawerActivity) activity;
    }

   /* private void setupMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                double lati = 0, longti = 0;
                mMap.clear();
                *//*if(listUsers!=null&&listUsers.size()>0){
                    for (User user : listUsers) {
						lati = Double.parseDouble(user.latitude);
						longti = Double.parseDouble(user.longitude);
						map.addMarker(new MarkerOptions().position(new LatLng(lati,longti ))
								.title(user.fname+" "+user.lname).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));
						builder.include(new LatLng(lati, longti));
					}
				}*//*
              *//*  if (loc != null) {
                    if ((loc.getLatitude() != 0D) && (loc.getLongitude() != 0D)) {
                        lati = loc.getLatitude();
                        longti = loc.getLongitude();
                        mMap.addMarker(new MarkerOptions().position(new LatLng(lati, longti))
                                .title("Your location"));
                        builder.include(new LatLng(lati, longti));
                        LatLngBounds bounds = builder.build();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lati, longti), 11.0f));
                    }
                }*//*
            }
        });
    }*/

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_setting) {
            String[] array = getResources().getStringArray(R.array.places_array);
            AlertDialogViewFreqAct(array, "Select Places to search", true);

        }
        return false;
    }*/

  /*  public Location getLocation() {
        Location location = null;
        boolean isGPSEnabled = false;
        boolean isNetworkEnabled = false;
        try {

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                if (mShowGpsOnDialog) {
                    //showGPSDisabledAlertToUser();
                    settingsrequest();
                }
                // no network provider is enabled
            } else {
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME,
                            MIN_DISTANCE, this);
                    Log.e("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            loc = location;
                            mShowGpsOnDialog = false;
                            Log.e("Location", "From network");
                            // latitude = location.getLatitude();
                            // longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME,
                                MIN_DISTANCE, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                mShowGpsOnDialog = false;
                                loc = location;
                                Log.e("Location", "From GPS");
                                // latitude = location.getLatitude();
                                // longitude = location.getLongitude();
                            }
                        }
                    }
                }

                if (location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        loc = location;
                        Log.e("Location", "getLastKnownLocation");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (location != null) {
            loc = location;
            // new SearchUserNearBy().execute(location.getLatitude()+"", location.getLongitude()+"",Constants.RADIUS,"normal");
            //new AsyncUpdateLatLong().execute(location.getLatitude()+"", location.getLongitude()+"");
            locationManager.removeUpdates(this);
        }
        return location;
    }*/

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        LatLng sydney;
        // Add a marker in Sydney and move the camera
        sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-34, 151), 11.0f));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (isVisible())
            new AsyncGetPlaces(true).execute(orderId);
    }


    /*
    @Override
    public void onResume() {
        super.onResume();
        if(isVisible()&&mMap!=null){
            new AsyncGetPlaces(true).execute(orderId);
        }
    }*/

    int FreqPosition = -1;
    int currItemFreqAct = -1;

  /*  private void AlertDialogViewFreqAct(final String[] items, String Title, final boolean isfreq) {
        int position = -1;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(Title);
        position = FreqPosition;
        builder.setSingleChoiceItems(items, position,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item >= 0) {
                            currItemFreqAct = item;
                        }
                    }
                });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if ((currItemFreqAct != FreqPosition) && (currItemFreqAct >= 0)) {
                    FreqPosition = currItemFreqAct;
                    try {
                        //t.setText(items[currItemFreqAct]);
                        String type=items[currItemFreqAct].toLowerCase();
                        if(type.contains(" "))
                            type = type.replace(" ","_");
                        new AsyncGetPlaces().execute(loc.getLatitude() + "", loc.getLongitude() + "",
                                type);
                        //TODO: call server here.
                    } catch (ArrayIndexOutOfBoundsException e) {

                    }
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }*/

    /* @Override
     public void onLocationChanged(Location location) {
         loc = location;
         setupMap();
     }

     @Override
     public void onStatusChanged(String s, int i, Bundle bundle) {

     }

     @Override
     public void onProviderEnabled(String s) {

     }

     @Override
     public void onProviderDisabled(String s) {

     }

     public void settingsrequest() {
         mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                 .addApi(LocationServices.API)
                 *//*.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)*//*.build();
        mGoogleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                if (status != null) {
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(mActivity, Constants.REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

*/
    ProgressDialog mProgressDialog;
    Handler h = new Handler();
    final int delay = 1000; //milliseconds
    Runnable finalizer = new Runnable() {
        public void run() {
            new AsyncGetPlaces(false).execute(orderId);
            h.postDelayed(this, delay);
        }
    };

    private class AsyncGetPlaces extends AsyncTask<String, Long, String> {
        boolean isForeground;

        public AsyncGetPlaces(boolean isForeground) {
            this.isForeground = isForeground;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isForeground) {
                if (mProgressDialog == null)
                    mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setMessage("Getting location");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        protected String doInBackground(String... urls) {
            try {
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "qdridergetorderlatlong", "order_id", urls[0]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                if (isForeground)
                    mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject obj = new JSONObject(result);
                    if (obj.getString("error").equalsIgnoreCase("0")) {
                        JSONObject resultArray = obj.getJSONObject("data");
                        if (!isForeground) {
                            mMap.clear();

                        }
                        // builder = new LatLngBounds.Builder();
                        Double lat, lng;
                        lat = Double.parseDouble(resultArray.getString("latitude"));
                        lng = Double.parseDouble(resultArray.getString("longtitude"));
                        mMap.addMarker(new MarkerOptions().position(
                                new LatLng(lat, lng))
                                .title("Rider Location\n" + resultArray.getString("first_name"))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin))
                        );
                        // builder.include(new LatLng(lat, lng));
                        // builder.build();
                        //   LatLng sydney = new LatLng(lat, lng);
                        if (isForeground) {
                            // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 11.0f));

                           /* mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                @Override
                                public void onMapLoaded() {
                                    //LatLng sydney;
                                    // Add a marker in Sydney and move the camera
        *//*if (Detail.loc != null) {
            sydney = new LatLng(Detail.loc.getLatitude(), Detail.loc.getLongitude());
        } else*//*
                                    //  sydney = new LatLng(-34, 151);
                                    //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                                    //  loc = getLocation();
                                    //if (loc != null) {
                                    //setupMap();
                                    new AsyncGetPlaces(true).execute(orderId);
                                }
                            });*/


                            h.postDelayed(finalizer, delay);

                        } else {
                            LatLng sydney = new LatLng(lat, lng);
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        }
                    }
                } else {

                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (h != null) {
            h.removeCallbacks(finalizer);
            h = null;
        }
    }

}
