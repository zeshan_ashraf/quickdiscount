package com.xintsolutions.quickdiscount.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.CheckoutAct;
import com.xintsolutions.quickdiscount.MainActivity;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.model.UserInfo;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Asad Waheed on 10/9/2016.
 */

public class PaymentMethod extends AppCompatActivity {

    AppCompatActivity mActivity;
    UserInfo info;
    int total;
    String json;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment_method);
        mActivity = this;
        Button btnCashOnDelivery = (Button) findViewById(R.id.btnCashOnDelivery);
        Button btnCreditCard = (Button) findViewById(R.id.btnCreditCard);
        btnCreditCard.setOnClickListener(clickListener);
        btnCashOnDelivery.setOnClickListener(clickListener);
        info = (UserInfo) getIntent().getExtras().getSerializable("data");
        List<Product> listTips = Settings.getCartList(mActivity);
        int subTotal = 0, handlingCharges = 0;
        if (listTips == null)
            listTips = new ArrayList<>();
        int size = listTips.size();
        json = "[";
        for (int i = 0; i < size; i++) {
            Product product = listTips.get(i);
            if (i != (size - 1))
                json = json + "{\"product_id\":" + product.id + ",\"quantity\":" + product.quantity + "},";
            else
                json = json + "{\"product_id\":" + product.id + ",\"quantity\":" + product.quantity + "}]";
            subTotal = subTotal + product.priceOfOrder;
        }
        if (subTotal >= 5000) {
            handlingCharges = 0;
        } else {
            handlingCharges = 100;
        }
        total = subTotal + handlingCharges;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return false;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnCreditCard:
                    Intent intent = new Intent(mActivity, PayPalActivity.class);
                    intent.putExtra("name", "Groceries");
                    String dollarTotal = String.valueOf(total / 102);
                    intent.putExtra("price", dollarTotal);
                    mActivity.startActivityForResult(intent, 22);
                    break;
                case R.id.btnCashOnDelivery:

                    new AsyncCheckOut().execute(info.fname, info.lname, info.address, info.address2, info.city, info.state,
                            info.postal_code, info.country, info.email_address, info.phone_number, json, total + "",
                            Settings.getUserId(mActivity));
                    break;

            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 22) {
            if ("paid".equals(data.getExtras().getString("payment"))) {
                new AsyncCheckOut().execute(info.fname, info.lname, info.address, info.address2, info.city, info.state,
                        info.postal_code, info.country, info.email_address, info.phone_number, json, total + "",
                        Settings.getUserId(mActivity));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    ProgressDialog mProgressDialog;

    private class AsyncCheckOut extends AsyncTask<String, Long, String> {
        public String phone;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                phone = urls[0];
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "quickdiscount_create_order", "payment_method", "cod", "payment_method_title",
                        "Cash On Delivery", "set_paid", "false", "first_name", urls[0], "last_name", urls[1],
                        "address_1", urls[2], "address_2", urls[3], "city", urls[4], "state", urls[5], "postcode",
                        urls[6], "country", urls[7], "email", urls[8], "phone", urls[9], "line_item", urls[10],
                        "total_amount", urls[11], "customer_id", urls[12]);
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Settings.setCartList(mActivity, new ArrayList<Product>());
                        Intent intent = new Intent(mActivity, DrawerActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        /*Settings.setIsUserLoggedIn(true, mActivity);
                        JSONObject msgObj = jsonObject.getJSONObject("user");
                        User user = new User();
                        user.user_id = msgObj.getString("user_id");
                        user.email_address = msgObj.getString("email_address");
                        user.user_name = msgObj.getString("user_name");
                        user.address = msgObj.getString("address");
                        user.phone_number = msgObj.getString("phone_number");
                        Settings.setUserId(user.user_id, mActivity);
                        Settings.setUser(mActivity, user);
                        startActivity(new Intent(mActivity, DrawerActivity.class));
                        finish();*/
                    } else {
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
