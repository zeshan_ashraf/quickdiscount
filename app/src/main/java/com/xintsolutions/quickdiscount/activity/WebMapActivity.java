package com.xintsolutions.quickdiscount.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.TextView;

import com.xintsolutions.quickdiscount.R;

public class WebMapActivity extends AppCompatActivity {
    final String BASEURL = "http://www.booking.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_map);
        WebView webView = (WebView) findViewById(R.id.webView);
        final TextView loading = (TextView) findViewById(R.id.tvLoading);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        String url = BASEURL;// + "origin="+from+"&destination="+to;
        webView.loadUrl(url);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                loading.setText("Loading... " + progress + "%");
                if (progress == 100)
                    loading.setVisibility(View.GONE);
            }
        });


    }
}
