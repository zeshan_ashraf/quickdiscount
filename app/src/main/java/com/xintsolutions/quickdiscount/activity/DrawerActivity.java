package com.xintsolutions.quickdiscount.activity;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.xintsolutions.quickdiscount.CheckoutAct;
import com.xintsolutions.quickdiscount.Splash;
import com.xintsolutions.quickdiscount.fragment.Favorites;
import com.xintsolutions.quickdiscount.MainScreen;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.fragment.MapFrg;
import com.xintsolutions.quickdiscount.fragment.MeFrg;
import com.xintsolutions.quickdiscount.fragment.ProfileFrg;
import com.xintsolutions.quickdiscount.model.Product;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.utils.Settings;
import com.xintsolutions.quickdiscount.utils.Utils2;

import java.util.List;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public TextView tvTitle;
    AppCompatActivity mActivity;
    public LayerDrawable icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        mActivity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
        // actionBar.setLogo(R.mipmap.logo_action_bar);
        //actionBar.setDisplayUseLogoEnabled(true);
        // actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setDisplayShowTitleEnabled(false);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();


        // find MenuItem you want to change
        MenuItem logOut = menu.findItem(R.id.nav_manage);
        if (Settings.getIsUserLoggedIn(mActivity)) {
            logOut.setTitle("Log Out");
            View header = navigationView.getHeaderView(0);
            User user = Settings.getUser(mActivity);
            TextView tvUserName = (TextView) header.findViewById(R.id.tvUserName);
            TextView tvUserAddress = (TextView) header.findViewById(R.id.tvUserAddress);
            tvUserName.setText(user.user_name);
            tvUserAddress.setText(user.address);
        } else {
            logOut.setTitle("Login");
        }
        navigationView.setNavigationItemSelectedListener(this);
        findViewById(R.id.tvUserName);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                new MainScreen())
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Product> lis = Settings.getCartList(mActivity);
        if (lis != null)
            // Update LayerDrawable's BadgeDrawable
            Utils2.setBadgeCount(this, icon, lis.size());
        else
            Utils2.setBadgeCount(this, icon, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        // Get the notifications MenuItem and LayerDrawable (layer-list)
        MenuItem item = menu.findItem(R.id.action_settings);
        icon = (LayerDrawable) item.getIcon();
        List<Product> lis = Settings.getCartList(mActivity);
        if (lis != null)
            // Update LayerDrawable's BadgeDrawable
            Utils2.setBadgeCount(this, icon, lis.size());
        else
            Utils2.setBadgeCount(this, icon, 0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, CheckoutAct.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                    new MainScreen())
                    .commit();
        } else if (id == R.id.nav_gallery) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                    new Favorites())
                    .commit();
        } else if (id == R.id.nav_profile) {
            if (Settings.getIsUserLoggedIn(mActivity))
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                        new ProfileFrg())
                        .commit();
            else {
                Toast.makeText(mActivity, "Please login to see your profile.", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_manage) {
            if (Settings.getIsUserLoggedIn(mActivity)) {
                Settings.setIsUserLoggedIn(false, mActivity);
                Settings.setUser(mActivity, null);
                Settings.setUserId("", mActivity);
                Intent intent = new Intent(mActivity, Splash.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                mActivity.finish();
            } else {
                Intent intent = new Intent(mActivity, Splash.class);
                startActivity(intent);
                mActivity.finish();
            }

        } else if (id == R.id.nav_slideshow) {
            mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                    new MeFrg()).commit();

        } else if (id == R.id.radar) {
            mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containers,
                    new MapFrg()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
