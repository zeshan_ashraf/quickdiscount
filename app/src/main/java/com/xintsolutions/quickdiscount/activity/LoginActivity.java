package com.xintsolutions.quickdiscount.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonSyntaxException;
import com.xintsolutions.quickdiscount.R;
import com.xintsolutions.quickdiscount.model.User;
import com.xintsolutions.quickdiscount.utils.Constants;
import com.xintsolutions.quickdiscount.utils.Settings;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    AppCompatActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mActivity = this;
        final EditText etPhone = (EditText) findViewById(R.id.etEmail);
        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = etPhone.getText().toString();
                new AsyncLoginUser().execute(phone, Settings.getUDID(mActivity));
            }
        });
    }

    ProgressDialog mProgressDialog;

    private class AsyncLoginUser extends AsyncTask<String, Long, String> {
        public String phone;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.show();
        }

        protected String doInBackground(String... urls) {
            try {
                phone = urls[0];
                HttpRequest request = HttpRequest.get(Constants.BASE_URL, true,
                        "action", "register_user_phone", "phone", phone
               //         ,"udid", urls[1]
                );
                if (request.ok()) {
                    return request.body();
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String result) {
            try {
                mProgressDialog.dismiss();
                if (result != null) {
                    Log.e("MyApp", "response:  " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("error").equalsIgnoreCase("0")) {
                        Settings.setIsUserLoggedIn(true,mActivity);
                        JSONObject msgObj = jsonObject.getJSONObject("user");
                        User user = new User();
                        user.user_id=msgObj.getString("user_id");
                        user.phone_number=msgObj.getString("phone_number");
                        user.code=msgObj.getString("code");
                        Settings.setUserId(user.user_id,mActivity);
                        Settings.setUser(mActivity,user);
                        startActivity(new Intent(mActivity, DrawerActivity.class));
                        finish();
                    }else{
                        Toast.makeText(mActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection problem.", Toast.LENGTH_SHORT).show();
                    Log.e("MyApp", "Request Failed");
                }
            } catch (IllegalArgumentException e) {

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Toast.makeText(mActivity, "Invalid Response", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
